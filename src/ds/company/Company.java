package ds.company;

import ds.Employee.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by winfieldhill on 12/1/14.
 *
 */
public class Company {
    public Company() throws FileNotFoundException {
        run();
    }

    public impl_ftEmployee getNextEmployee(Scanner sc) {
        Scanner lineScanner = new Scanner(sc.nextLine());
        String fName = lineScanner.next();
        String lName = lineScanner.next();
        double grossPay = lineScanner.nextDouble();

        return new impl_ftEmployee(fName, lName, grossPay);
    }

    public impl_ftEmployee findBestPaid(Scanner sc) {
        impl_ftEmployee full,
                bestPaid = new impl_ftEmployee("", "", 0.00);

        while (sc.hasNext()) {
            full = getNextEmployee(sc);

            if (full.grossPay > bestPaid.grossPay) {
                bestPaid = full;
            }
        }

        if (bestPaid.grossPay == 0.00){
            return null;
        }

        return bestPaid;
    }

    public void run() throws FileNotFoundException {
        final String    INPUT_PROMPT = "Please enter the path for the file of employees: ",
                        BEST_PAID_MESSAGE = "\n\nThe best paid employee is: ",
                        NO_INPUT_MESSAGE = "\n\nError: There were no employees scanned in.";

        System.out.print(INPUT_PROMPT);

        String fileName = new Scanner(System.in).nextLine();
        Scanner sc = new Scanner(new File(fileName));

        impl_ftEmployee bestPaid = findBestPaid(sc);

        if (bestPaid == null) {
            System.out.println(NO_INPUT_MESSAGE);
        }
        else {
            System.out.println(BEST_PAID_MESSAGE + bestPaid.toString(0));
        }
    }
}
