package ds.Employee;

/**
 * Created by winfieldhill on 12/2/14.
 *
 */
public interface i_HourlyEmployee {
    final static double MAX_REGULAR_HOURS = 40.0,
                        OVERTIME_FACTOR = 1.5;

    void setHoursWorked( double hw );
    void setPayRate( double pr );
}
