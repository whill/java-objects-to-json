package ds.Employee;

import crockford.json.JSONStringer;

/**
 * Created by winfieldhill on 12/2/14.
 *
 */
public class impl_HourlyEmployee extends impl_ftEmployee implements i_HourlyEmployee {
    private double  hoursWorked = 0.00,
                    payRate = 7.50;

    public impl_HourlyEmployee(String fName, String lName, double pr, double hw) {
        super(fName, lName, 0);
        setHoursWorked(hw);
        setPayRate(pr);
    }

    public void setHoursWorked(double hw) {
        this.hoursWorked = hw;
    }

    public void setPayRate(double pr) {
        if (this.hoursWorked > MAX_REGULAR_HOURS) {
            double rate = pr * OVERTIME_FACTOR;
            this.payRate = (this.hoursWorked - 40) / this.hoursWorked * rate + 40 / this.hoursWorked * pr;
        }
        else {
            this.payRate = pr;
        }
    }

    public double getHoursWorked() {
        return this.hoursWorked;
    }

    public double getAmountPaidThisWeek() {
        return this.hoursWorked * this.payRate;
    }

    @Override
    public String toString() {
        return new StringBuilder()
            .append("'").append(new JSONStringer()
                .object()
                    .key("Employee").object()
                    .key("firstName").value(this.firstName)
                    .key("lastName").value(this.lastName)
                    .key("hoursWorked").value(getHoursWorked())
                    .key("payThisWeek").value(MONEY.format(getAmountPaidThisWeek()))
                    .endObject()
                .endObject())
            .append("'").toString();
    }
}
