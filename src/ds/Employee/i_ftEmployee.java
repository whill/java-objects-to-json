package ds.Employee;

import java.text.DecimalFormat;

/**
 * Created by winfieldhill on 11/29/14.
 *
 */
public interface i_ftEmployee {

    final static DecimalFormat MONEY = new DecimalFormat(" $0.00");

    /**
     *
     * @return a String of the Fulltime Employee object's name.
     */
    String getName();

    /**
     *
     * @return a String representing the Fulltime Employee object's gross pay.
     */
    String getGrossPay();

    /**
     *
     * @return a String representation of this Fulltime Employee object.
     */
    @Override
    String toString();
} // Interface ftEmployee
