package ds.Employee;

import crockford.json.*;
/**
 * Created by winfieldhill on 11/29/14.
 *
 */
public class impl_ftEmployee implements i_ftEmployee {

    String  firstName,
            lastName;
    public double
            grossPay;

    public impl_ftEmployee() {
        setFirstName("");
        setLastName("");
        setGrossPay(0.00);
    }

    public impl_ftEmployee(String fName, String lName, double gPay) {
        setFirstName(fName);
        setLastName(lName);
        setGrossPay(gPay);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setGrossPay(double grossPay) {
        this.grossPay = grossPay;
    }

    @Override
    public String getName() {
        return this.firstName + " " + this.lastName;
    }

    @Override
    public String getGrossPay() {
        return MONEY.format(this.grossPay);
    }

    @Override
    public String toString() {
        return "'{\"Employee\" : {\"firstName\" : \"" + this.firstName + "\", \"lastName\" : \"" + this.lastName +
                "\", " + "\"grossPay\" : \"" + this.grossPay + "\"}}'";
    }

    public String toString(int i) {
        return "'" + (new JSONStringer()
                .object()
                .key("Employee").object()
                .key("firstName").value(this.firstName)
                .key("lastName").value(this.lastName)
                .key("grossPay").value(this.getGrossPay())
                .endObject()
                .endObject()
        ) + "'";
    }
}
