package ds.abstractMethodsAndInterfaces.p28_31.abstracts;

import ds.abstractMethodsAndInterfaces.p28_31.interfaces.*;

/**
 * Created by winfieldhill on 11/27/14.
 *
 */
public abstract class a_P28 implements i_P28 {

    public void draw(int x, int y, int r) {}

    public void move(int x, int y) {}
}
