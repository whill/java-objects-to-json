package ds.abstractMethodsAndInterfaces.p28_31.interfaces;

import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by winfieldhill on 11/29/14.
 *
 */
public interface i_P31 {

    final static DecimalFormat MONEY = new DecimalFormat(" $0.00");

    String getEmployeeName();
    AtomicLong getEmployeeId();
    String getEmployeePay();
    String toString();
}
