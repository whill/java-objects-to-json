package ds.abstractMethodsAndInterfaces.p28_31.interfaces;

/**
 * Created by winfieldhill on 11/27/14.
 *
 */
public interface i_P28 {
    final static int
        c = 0,
        MAX_X_CORD = 1024,
        MAX_Y_CORD = 768;

    /**
     * Draws this P28 Figure at given coordinates.
     *
     * @param x - the X coordinate of center point for Figure.
     * @param y - the Y coordinate of center point for Figure.
     */
    void draw(int x, int y, int r);

    /**
     * Moves this P28 Figure to the coordinates specified.
     *
     * @param x - the x coordinate of center point to move this Figure to.
     * @param y - the y coordinate of center point to move this Figure to.
     */
    void move(int x, int y);
}
