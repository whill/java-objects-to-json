package ds.abstractMethodsAndInterfaces.p28_31.impl;

import ds.abstractMethodsAndInterfaces.p28_31.abstracts.*;
import ds.abstractMethodsAndInterfaces.p28_31.interfaces.*;

/**
 * Created by winfieldhill on 11/29/14.
 * Implementation class
 */
public class P28Impl extends a_P28 implements i_P28 {

    private int xCoord,
                yCoord,
                radius;

    public P28Impl(int x, int y, int r) {

        draw(x, y, r);
        System.out.println("Circle drawn at " + x + ", " + y + " with a radius of " + r);
        //System.out.println(i_P28.c + " - " + i_P28.MAX_X_CORD + " - " + i_P28.MAX_Y_CORD);
    }

    @Override
    public void draw(int x, int y, int r) {

        this.xCoord = x;
        this.yCoord = y;
        this.radius = r;
    }

    @Override
    public void move(int x, int y) {

        System.out.println("Moving the Circle drawn at " + this.xCoord + ", " + this.yCoord + " with a radius of " + this
                .radius + " from " + this.xCoord + ", " + this.yCoord + " to ");
        this.xCoord += x;
        this.yCoord += y;

        System.out.println(this.xCoord + ", " + this.yCoord);
    }
}
