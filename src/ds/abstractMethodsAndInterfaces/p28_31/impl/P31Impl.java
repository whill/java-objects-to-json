package ds.abstractMethodsAndInterfaces.p28_31.impl;

import ds.abstractMethodsAndInterfaces.p28_31.interfaces.*;
import ds.abstractMethodsAndInterfaces.p28_31.abstracts.*;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by winfieldhill on 11/29/14.
 *
 */
public class P31Impl extends a_P31 implements i_P31 {

    private final AtomicLong employeeId = new AtomicLong( (new Random()).nextLong() );
    private final String employeeName;
    private final double employeePay;

    public String getEmployeeName() {
        return this.employeeName;
    }

    public AtomicLong getEmployeeId() {
        return this.employeeId;
    }

    public String getEmployeePay() {
        return MONEY.format(this.employeePay);
    }

    public P31Impl(String name, double pay) {
        this.employeeName = name;
        this.employeePay = pay;
    }
}
