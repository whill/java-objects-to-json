package ds.abstractMethodsAndInterfaces.p28_31.impl;

import ds.abstractMethodsAndInterfaces.p28_31.abstracts.a_P28;
import ds.abstractMethodsAndInterfaces.p28_31.interfaces.i_P28;

/**
 * Created by winfieldhill on 11/27/14.
 *
 */
public class P28 extends a_P28 {

    public P28() {

        System.out.println(i_P28.c + " - " + i_P28.MAX_X_CORD + " - " + i_P28.MAX_Y_CORD);
    }

    public void draw(int x, int y) {

    }
}
