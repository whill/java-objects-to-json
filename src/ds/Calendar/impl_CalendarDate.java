package ds.Calendar;

/**
 * Created by winfieldhill on 12/5/14.
 *
 */
public class impl_CalendarDate implements i_CalendarDate{
    private int defaultYear,
                defaultDay;

    private String defaultMonth;

    public impl_CalendarDate() {
        this.defaultDay = 1;
        this.defaultYear = 2012;
        this.defaultMonth = i_CalendarDate.MONTHS[0];
    }

    public int getCurrentYear() {
        return i_CalendarDate.year;
    }

    public boolean isLeapYear() {
        return i_CalendarDate.isLeapYear();
    }

    public int getDaysThisMonth() throws ArrayIndexOutOfBoundsException {
        if (isLeapYear() && i_CalendarDate.month == 2) {
            return 29;
        } else {
            return i_CalendarDate.daysPerMonth[i_CalendarDate.month];
        }
    }

    public String getDefaultMonth() {
        return defaultMonth;
    }

    public void setDefaultMonth(String defaultMonth) {
        this.defaultMonth = defaultMonth;
    }
}
