package ds.Calendar;

import java.time.LocalDateTime;
import java.time.Year;

/**
 * Created by winfieldhill on 12/5/14.
 *
 */
public interface i_CalendarDate {
    static int  year = Integer.parseInt(Year.now().toString()),
                month = (LocalDateTime.now()).getMonthValue() - 1;

    static int[] daysPerMonth = {
            31,
            28,
            31,
            30,
            31,
            30,
            31,
            31,
            30,
            31,
            30,
            31
    };

    static String[] MONTHS = {
            "january",
            "february",
            "march",
            "april",
            "may",
            "june",
            "july",
            "august",
            "september",
            "october",
            "november",
            "december"
    };

    static boolean isLeapYear() {
        return ((i_CalendarDate.year % 4) == 0)
                    && (((i_CalendarDate.year % 100) == 0) && ((i_CalendarDate.year % 400) == 0));
    }
}
