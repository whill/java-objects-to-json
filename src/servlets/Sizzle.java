package servlets;

import java.io.IOException;

import ds.Calendar.impl_CalendarDate;
import ds.Employee.*;

/**
 * Created by winfieldhill on 11/22/14.
 *
 * Servlet class to test DSs
 */
public class Sizzle extends javax.servlet.http.HttpServlet {

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        request.setAttribute("name", "Win");
        //request.getRequestDispatcher("sizzle.jsp").forward(request, response);

        //P28Impl p28 = new P28Impl(50, 150, 30);
        //p28.move(100, 50);
        //P31Impl p31 = new P31Impl("Winfield Hill", 1000000.00);
        //System.out.println(p31.getEmployeeId() + " - " + p31.getEmployeeName() + " - " + p31.getEmployeePay());
        impl_ftEmployee ftEmployee = new impl_ftEmployee("Win", "Hill", 500000);
        //System.out.println(ftEmployee.toString());
        System.out.println(ftEmployee.toString(0));

        impl_HourlyEmployee hrEmployee = new impl_HourlyEmployee("Win", "Hill", 75, 50);
        System.out.println(hrEmployee.toString());
        //Company co = new Company();
        //co.run();
        impl_ftEmployee anEmployee;
        anEmployee = new impl_HourlyEmployee("Win", "Hill", 50, 50);
        System.out.println("0 - " + anEmployee.toString() + (anEmployee.getClass().toString()));
        anEmployee = ftEmployee;
        System.out.println("1 - " + anEmployee.toString(0) + (anEmployee.getClass().toString()));

        doPost(request, response);
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        request.setAttribute("name_l", "Hill");
        request.getRequestDispatcher("sizzle.jsp").forward(request, response);

        impl_CalendarDate cd1 = new impl_CalendarDate();
        System.out.println(cd1.getCurrentYear());
        System.out.println(cd1.isLeapYear());
        System.out.println(cd1.getDaysThisMonth());
    }
}
